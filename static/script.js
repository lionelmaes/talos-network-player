const SocketManager = {
    socket:null,
    nodes:[],
    
    $nodes:document.getElementById('nodes'),
    $scenarios:document.getElementById('scenarios'),
    $playerScenarios:document.getElementById('player-scenarios'),

    insertNewNode(data){
        SocketManager.nodes.push(data);
        $node = document.createElement('li');
        $node.classList.add('node');
        $node.setAttribute('data-serverIP', data['serverIP']);
        $node.innerHTML = data['serverHostName'];
        SocketManager.$nodes.appendChild($node);
        MountingTable.updateNodeList(SocketManager.nodes);
    },
    saveScenario(scenario){
        console.log(scenario);
        SocketManager.socket.emit('save_scenario', scenario);
    },
    loadScenario(file){
        SocketManager.socket.emit('load_scenario', {'file':file});
    },
    importScenario(file){
        SocketManager.socket.emit('import_scenario', {'file':file});
    },
    playFile(file){
        SocketManager.socket.emit('play_file', {file:file});
    },
    init(){
        SocketManager.socket = io.connect('http://' + document.domain + ':' + location.port);
        SocketManager.socket.on('new_node', function(data){
            if(SocketManager.nodes.map(function(e) { return e.serverIP; }).indexOf(data['serverIP']) == -1){
                SocketManager.insertNewNode(data);
            }
            
        });
        SocketManager.socket.on('node_list', function(data){
            SocketManager.$nodes.innerHTML = '';
            SocketManager.nodes.length = 0;
            for(var i in data){
                console.log(data[i]);
                SocketManager.insertNewNode(data[i]);
            }
            console.log(data);
        });
        SocketManager.socket.on('save_scenario', function(data){
            console.log('received new scenario');
            console.log(data);
            let $option = document.createElement('option');
            $option.innerText = data.name+'.json';
            $option.value = data.name+'.json';
            console.log(SocketManager.$scenarios.querySelector('option[value="'+$option.value+'"]'));
            if(SocketManager.$scenarios.querySelector('option[value="'+$option.value+'"]') == null){
                SocketManager.$scenarios.appendChild($option);
                SocketManager.$scenarios.value = $option.value;
            }
            
            if(SocketManager.$playerScenarios.querySelector('option[value="'+$option.value+'"]') == null)
                SocketManager.$playerScenarios.appendChild($option.cloneNode(true));
            
            
        });

        SocketManager.socket.on('load_scenario', function(data){
            ScenarioManager.loadScenario(data);
        });
        SocketManager.socket.on('import_scenario', function(data){
            ScenarioManager.importScenario(data);
        });
        SocketManager.socket.emit('interface_connection');        
    }

}






const UI = {

    $UIS:[],

    update:($root) => {
        let $NewUIS;
        console.log($root);
        if($root.classList != undefined && $root.classList.contains('ui'))
            $NewUIS = [$root];
        
        else
            $NewUIS = $root.querySelectorAll('.ui');
        
        for(let i = 0; i < $NewUIS.length; i++){
            let context = $NewUIS[i].getAttribute('data-context');
            let $buttons = $NewUIS[i].querySelectorAll('button');
            $buttons.forEach($button => {
                $button = UI.removeListeners($button);
                
                $button.addEventListener('click', (e) => {
                    UI.doSomething(context, $button.getAttribute('data-action'), $button.value);
                    
                });
            });
            let $inputs = $NewUIS[i].querySelectorAll('input');
            $inputs.forEach($input => {
                $input = UI.removeListeners($input);
                $input.addEventListener('change', (e) => {
                    e.preventDefault();
                    UI.doSomething(context, $input.getAttribute('data-action'), $input.value);
                });

            });
            let $selects = $NewUIS[i].querySelectorAll('select');
            $selects.forEach($select => {
                $select = UI.removeListeners($select);
                $select.addEventListener('change', (e) => {
                    if($select.value != undefined)
                        UI.doSomething(context, $select.getAttribute('data-action'), $select.value, $select.getAttribute('data-key'), $select.options[$select.selectedIndex].text);
                });
            });
            let $sources = $root.querySelectorAll('#sources li');
            $sources.forEach($source => {
                $source = UI.removeListeners($source);
                $source.addEventListener('dragstart', UI.sourceDragStart);


            });
            
           
        }

    },

    removeListeners:($element) => {
        let $newElem = $element.cloneNode('true');
        $element.parentNode.replaceChild($newElem, $element);
        return $newElem;
    },

    sourceDragStart:(e) => {
        e.dataTransfer.effectAllowed = 'move';
        UI.$draggedElement = e.target;
        console.log(e.target);
        //e.target.style.background = "red";
    },

    setInput:(context, action, value) => {
        let $element = document.querySelector('*[data-context='+context+'] *[data-action='+action+']');
        $element.value = value;
    },
    getInput:(context, action) => {
        console.log(context+' '+action);
        let $element = document.querySelector('*[data-context='+context+'] *[data-action='+action+']');
        return $element.value;
    },
    
    doSomething:(context, action, value = null, key = null, text = null) => {
        
        switch(context){
            case 'player':
                switch(action){
                    case 'play':
                        SocketManager.playFile(value);
                        console.log(value);
                        return;
                }
                return;
            case 'scenario':
                switch(action){
                    case 'new':
                        MountingTable.reset();
                        let duration = prompt('Enter duration');
                        let scale = UI.getInput('mounting-table', 'scale');
                        UI.setInput('mounting-table', 'duration', duration);
                        MountingTable.initDurationAndScale(duration, scale);
                        MountingTable.show();
                        
                        return;
                    case 'save':
                        
                        let scenarioName = '';
                        if(ScenarioManager.scenario != undefined)
                            scenarioName = prompt('Enter scenario name', ScenarioManager.scenario.name);
                        else
                            scenarioName = prompt('Enter scenario name');
                        if(scenarioName != null){//if cancel prompt, do nothing
                            ScenarioManager.saveScenario(scenarioName);
                            SocketManager.saveScenario(ScenarioManager.scenario.returnObj);
                        }
                        return;
                    case 'load':
                        SocketManager.loadScenario(value);
                        return;
                } 
                return;
            case 'mounting-table':
                switch(action){
                    case 'new':
                        console.log('new track');
                        MountingTable.addTrack();
                        return;
                    case 'scale':
                        MountingTable.setScale(value);
                        return;
                    case 'duration':
                        MountingTable.setDuration(value);
                       
                }
                return;
            case 'track':
                switch(action){
                    case 'remove':
                        MountingTable.removeTrack(value);
                        return;
                    case 'select-actor':
                        MountingTable.setActorForTrack(key, value, text);
                        return;
                }
        }
    },

};

const MountingTable = {
    $mountingTable: document.querySelector('#mounting-table'),
    $tracks: document.querySelector('#tracks'),
    $timelineUI: document.querySelector('#timeline-ui'),
    $timelineLog: document.querySelector('#timeline-log'),
    $timeline: document.querySelector('#timeline'),
    $tracksUI: document.querySelector('#tracks-ui'),
    $scale: document.querySelector('#scale'),
    $dialog: document.querySelector('#mounting-table-dialog'),
    clipBoard: {'sources':[]},
    tracks:[],
    tracksCounter:0,
    show : () => {
        MountingTable.$mountingTable.style.display="block";
    },
    
    addTrack : () => {
        let track = MountingTable.trackFactory({id:MountingTable.tracksCounter});
        MountingTable.tracksCounter++;
        console.log('addtrack');
        track.$trackUI = MountingTable.addTrackUI(track);
        track.init();
        track.resizeElement(MountingTable.duration, MountingTable.scale);
        
        MountingTable.tracks.push(track);
        MountingTable.$tracks.appendChild(track.$track);
        return track;
        
    },

    addTrackUI : (track) => {
        console.log('track ui');
        let $trackUI = document.createElement('div');
        $trackUI.setAttribute('data-context', 'track');
        $trackUI.setAttribute('data-track-id', track.id);
        $trackUI.classList.add('track-ui');
        $trackUI.classList.add('ui');
        $trackUI.innerHTML = '<h6>track '+track.id+'</h6><select class="actors-list" data-key="'+track.id+'" '+
                            'data-action="select-actor"><option></option></select> '+
                            '<button data-action="remove" '+
                            'value="'+track.id+'">remove</button>';

        
        MountingTable.$tracksUI.appendChild($trackUI);
        MountingTable.populateTrackUI($trackUI, SocketManager.nodes);
        UI.update($trackUI);
        return $trackUI;

    },

    setDuration : (duration) => {
        MountingTable.duration = duration;
        MountingTable.setScale(MountingTable.scale);
        let $steps = MountingTable.$scale.querySelectorAll('.step');
        if($steps.length > duration){
            for(let i = $steps.length - 1; i >= duration; i--){
                $steps[i].parentNode.removeChild($steps[i]);
            }
        }else if($steps.length < duration){
            for(let i = $steps.length; i < duration; i++){
                MountingTable.addStep(i);
            }
        }
    },

    setScale : (scale) => {
        MountingTable.scale = scale;
        MountingTable.$scale.style.width = MountingTable.duration * scale +'px';
        MountingTable.$scale.querySelectorAll('.step').forEach($step => {
            $step.style.width = scale+'px';
        });
        
        MountingTable.tracks.forEach(track => {
            
            track.resizeElement(MountingTable.duration, MountingTable.scale);
        });
    },
    setActorForTrack: (trackID, actorID, actorName) => {
        MountingTable.tracks.forEach(track => {
            if(track.id == trackID){
                track.actorName = actorName;
                track.actorID = actorID;
                return;
            }
        });
    },
    reset: () => {
        MountingTable.$scale.innerHTML = '';
        MountingTable.$tracks.innerHTML = '';
        MountingTable.$tracksUI.innerHTML = '';
        MountingTable.tracks = [];
        MountingTable.tracksCounter = 0;
    },

    initDurationAndScale: (duration, scale) => {
        MountingTable.duration = duration;
        MountingTable.scale = scale;
        MountingTable.$scale.style.width = duration * scale +'px';
        for(let i = 0; i < duration; i++){
            MountingTable.addStep(i);
        }
    },
    addStep:(i) => {
        let $step = document.createElement('div');
        $step.classList.add('step');
        $step.setAttribute('data-i', i);
        let $stepInfo = document.createElement('span');
        $stepInfo.innerHTML = i+1;
        
        $step.style.width = MountingTable.scale+'px';
        $step.appendChild($stepInfo);
        MountingTable.$scale.appendChild($step);
    },
    populateTrackUI:($trackUI, nodes) => {
        console.log(nodes);
        let $actorsList = $trackUI.querySelector('select.actors-list');
        $actorsList.innerHTML = '<option></option>';
        nodes.forEach((node) => {
            let $option = document.createElement('option');
            $option.value = node['serverIP'];
            $option.innerText = node['serverHostName'];
            $actorsList.appendChild($option);
        });
        

    },
    removeTrack:(trackID) =>{
        console.log('ok');
        for(let i = 0; i < MountingTable.tracks.length; i++){
            console.log(trackID);
            if(MountingTable.tracks[i].id == trackID){
                MountingTable.tracks[i].destroy();
                let $trackUI = MountingTable.$tracksUI.querySelector('div[data-track-id="'+trackID+'"]');
                $trackUI.parentNode.removeChild($trackUI);
                MountingTable.tracks.splice(i, 1);
                break;
            }
        }
    },
    getTrackFromActorId:(actorID) => {
        for(let i = 0; i < MountingTable.tracks.length; i++){
            console.log('looking for'+actorID);
            console.log('actor id:'+MountingTable.tracks[i].actorID);
            
            if(MountingTable.tracks[i].actorID == actorID){

                return MountingTable.tracks[i];
            }
        }
        return false;
    },
    updateNodeList:(nodes) => {
        console.log(MountingTable.$tracksUI);
        console.log(nodes); 
        MountingTable.$tracksUI.querySelectorAll('.track-ui').forEach(($trackUI) => {
            MountingTable.populateTrackUI($trackUI, nodes);
        });
        
        console.log(nodes);
    },
    onAction:(action) => {
        //sending the action to all tracks
        MountingTable.tracks.forEach(track => {
            track.onAction(action);
        });
        switch(action){
            case 'copy':
                MountingTable.clipBoard.sources = [];
                MountingTable.copySourcesToClipBoard();
                console.log(MountingTable.clipBoard);
                break;
            case 'paste':
                MountingTable.pasteSourcesFromClipBoard();
                break;
            case 'import':
                MountingTable.dialog('import');
                break;
        }

               
    },
    pasteSourcesFromClipBoard: () => {
        MountingTable.clipBoard.sources.forEach(source => {
            if(MountingTable.tracks.length > source.track){
                let track = MountingTable.tracks[source.track];
                track.pasteSource(source);
                console.log('paste source in track ' + source.track);
            }else{
                console.log('track does not exist. no paste');
            
            }
        });
    },

    copySourcesToClipBoard: () => {
        MountingTable.tracks.forEach(track => {
            track.sources.forEach(source => {
                if(source.selected){
                    MountingTable.clipBoard.sources.push({
                        'track' : MountingTable.tracks.indexOf(track),
                        'type' : source.type,
                        'duration' : source.duration,
                        'position' : source.position,
                        'name' : source.name
                    });

                    
                }
            });
        });
    },
    unselectSources:() => {
        MountingTable.tracks.forEach(track => {
            track.sources.forEach(source => {
                source.unSelect();
            });
        });
    },

    dialog:(type) => {

        switch(type){
            case 'import':
                MountingTable.$dialog.style.display = "block";
                MountingTable.$dialog.innerHTML = '<h6>Import scenario: </h6>';
                let $scenarioSelect = document.querySelector('#scenario-ui select').cloneNode(true);
                
                MountingTable.$dialog.appendChild($scenarioSelect);
                $scenarioSelect.addEventListener('change', (e) => {
                    if($scenarioSelect.value != undefined)
                        SocketManager.importScenario($scenarioSelect.value);
                });

                break;
            
        }
    },
    onSourceMove:(e) => {
       
        let movement = e.clientX - MountingTable.cursorPos;
        MountingTable.cursorPos = e.clientX;
        MountingTable.tracks.forEach(track => {
            track.sources.forEach(source => {
                if(source.selected){
                    source.onElementMove(movement);
                }
            });
        });
        
        
            

        
    },

    init: () => {
        MountingTable.$tracks.addEventListener('scroll', (e) => {
            MountingTable.$tracksUI.style.top = -MountingTable.$tracks.scrollTop+'px';
            MountingTable.$scale.style.left = -MountingTable.$tracks.scrollLeft+'px';
        });
        MountingTable.$timeline.addEventListener('mousemove', (e) => {
            let rect = e.currentTarget.getBoundingClientRect();
            let x = e.clientX - rect.left; 
            //let y = e.clientY - rect.top;  //y position within the element.
            MountingTable.cursorPosition = x;
            let timePos = x + MountingTable.$tracks.scrollLeft;
            MountingTable.$timelineLog.innerHTML = '<p>'+timePos/MountingTable.scale+'s</p>';
            
        
        });
        MountingTable.$timeline.addEventListener('click', (e) => {
            MountingTable.$dialog.style.display = 'none';
            MountingTable.unselectSources();
        });

        
        
    },


    trackFactory : ({id}) => ({
        id,
        actorID: null,
        createElement(){
            this.$track = document.createElement('div');
            this.$track.classList.add('track');
            

            this.$audioTrack = document.createElement('div');
            this.$videoTrack = document.createElement('div');
            this.$audioTrack.classList.add('audio-track');
            this.$videoTrack.classList.add('video-track');
            
            this.$track.appendChild(this.$videoTrack);
            this.$track.appendChild(this.$audioTrack);
            
        },
        initEvents(){
            
            
            this.$track.addEventListener('dragenter', (e) => {
                //console.log(UI.$draggedElement.getAttribute('data-type'));
                console.log(e);
                
                this.$track.classList.add('focus');
                

                
            });
            this.$track.addEventListener('dragexit', (e) => {
                this.$track.classList.remove('focus');
                
            });
            this.$track.addEventListener('dragover', (e) => {
                e.preventDefault();
                //this.$track.style.background = 'gray';
            });

            this.$track.addEventListener('drop', (e) =>{
                console.log('drop');
                this.$track.classList.remove('focus');
                let rect = e.currentTarget.getBoundingClientRect();
                let x = (e.clientX - rect.left) / MountingTable.scale;
                this.addSource(
                    UI.$draggedElement.getAttribute('data-type'), 
                    UI.$draggedElement.getAttribute('data-duration'),
                    UI.$draggedElement.innerHTML,
                    x
                );
                
            });

        },

        addSource(type, duration, name, position){
            let source = MountingTable.sourceFactory({'type':type,
                                                      'duration':duration,
                                                      'name':name,
                                                      'position':position, 
                                                      'z-index':this.sources.length});
                
            source.init();
            if(source.type == 'video' || source.type == 'audio'){
                console.log(source.$audioElement);
                this.$audioTrack.appendChild(source.$audioElement);
                
            }
            if(source.type == 'image' || source.type == 'video'){
                console.log(this.$videoTrack);
                this.$videoTrack.appendChild(source.$imageElement);
            }
            
            this.sources.push(source);
        },
        pasteSource(sourceData){
            //compute position
            let position = 0;
            this.sources.forEach(source => {
                if(source.position + source.duration > position){
                    position = source.position + source.duration;
                }
            });
            console.log('adding source at position '+position);
            this.addSource(sourceData.type, sourceData.duration, sourceData.name, position);
        },
        resizeElement(duration, scale){
            this.$track.style.width = duration * scale +'px';
            this.sources.forEach((source) => {
                source.updateScale();
            });
        },

        onAction(action){
            switch(action){
                case 'delete':
                    this.sources.forEach((source, index, object) => {
                        if(source.selected){
                            source.destroy();
                            object.splice(index, 1);
                        }
                    });
                break;
            }
        },

        destroy(){
            this.$track.parentNode.removeChild(this.$track);
        },
        init(){
            this.sources = [];
            this.createElement();
            this.initEvents();

        }

    }),

    sourceFactory : ({type, duration, position, name}) => ({
        type,
        duration,
        position,
        name,

        selected:true,

        createElements(){
            this.$elements = [];
            if(this.type == 'audio' || this.type == 'video'){
                this.$audioElement = document.createElement('div');
                this.$audioElement.classList.add('source-audio');
                this.$audioElement.innerHTML = '<div class="left handle" data-direction="left"></div><div class="name">'+this.name+'</div><div data-direction="right" class="right handle"></div>';
                this.$audioElement.style.width = this.duration  * MountingTable.scale + 'px';
                this.$audioElement.style.left = this.position * MountingTable.scale+'px';
                this.$audioElement.classList.add('selected');
                this.$elements.push(this.$audioElement);
            }
            if(this.type == 'image' || this.type == 'video'){
                this.$imageElement = document.createElement('div');
                this.$imageElement.classList.add('source-image');
                this.$imageElement.innerHTML = '<div class="left handle" data-direction="left"></div><div class="name">'+this.name+'</div><div data-direction="right" class="right handle"></div>';
                this.$imageElement.style.width = this.duration  * MountingTable.scale + 'px';
                this.$imageElement.style.left = this.position * MountingTable.scale+'px';
                this.$imageElement.classList.add('selected');
                this.$elements.push(this.$imageElement);
            }
            //console.log(this.type);
        },

        initEvents(){
            this.cursorPos = 0;
            this.$elements.forEach(($element) => {
                
                this.onELementMoveBind = this.onElementMove.bind(this);
                $element.addEventListener('click', (e) => {
                    e.stopPropagation();
                });
                $element.addEventListener('mousedown', (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.select();

                    //this.cursorPos = e.clientX;
                    
                    //MountingTable.$mountingTable.addEventListener('mousemove', this.onELementMoveBind);
                    MountingTable.cursorPos = e.clientX;
                    MountingTable.$mountingTable.addEventListener('mousemove', MountingTable.onSourceMove);
                    
                    MountingTable.$mountingTable.addEventListener('mouseup', (e) => {
                        MountingTable.$mountingTable.onmouseup = null;
                        MountingTable.$mountingTable.removeEventListener('mousemove', MountingTable.onSourceMove);
                    });
                });
                

                let $handles = $element.querySelectorAll('.handle');
                $handles.forEach(($handle) => {
                    this.onElementResizeBind = this.onElementResize.bind(this);
                    $handle.addEventListener('mousedown', (e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        this.resizeDirection = $handle.getAttribute('data-direction');
                        console.log(this.resizeDirection);
                        this.cursorPos = e.clientX;
                        MountingTable.$mountingTable.addEventListener('mousemove', this.onElementResizeBind);
                        MountingTable.$mountingTable.addEventListener('mouseup', (e) => {
                            MountingTable.$mountingTable.onmouseup = null;
                            MountingTable.$mountingTable.removeEventListener('mousemove', this.onElementResizeBind);
                        });
                    });
                    
                });
                
            });

        },
        
        onElementResize(e){
            let movement = e.clientX - this.cursorPos;
            this.cursorPos = e.clientX;
            if(this.resizeDirection == 'left'){
                this.position = (this.position * MountingTable.scale + movement) / MountingTable.scale;
                this.duration =  (this.duration * MountingTable.scale - movement) / MountingTable.scale;
            }else{
                this.duration =  (this.duration * MountingTable.scale + movement) / MountingTable.scale;
            }
            this.$elements.forEach(($element) => {
                $element.style.left = this.position * MountingTable.scale + 'px';
                $element.style.width = this.duration  * MountingTable.scale + 'px';
            });


        },
        select(){
            console.log('select!');
            this.selected = true;
            this.$elements.forEach(($element) => {
                $element.classList.add('selected');
            });
        },
        unSelect(){
            this.selected = false;
            this.$elements.forEach(($element) => {
                $element.classList.remove('selected');
            });
        },
        updateScale(){
            this.$elements.forEach(($element) => {
                $element.style.left = this.position * MountingTable.scale + 'px';
                $element.style.width = this.duration * MountingTable.scale +'px';
            });
        },
        onElementMove(movement){
            
            this.position = (this.position * MountingTable.scale + movement) / MountingTable.scale;
            
            this.$elements.forEach(($element) => {
                $element.style.left = this.position * MountingTable.scale +'px';
            });
            

        },
        init(){
            this.createElements();
            this.initEvents();
        },
        destroy(){
            
            if(this.$audioElement != undefined){
                this.$audioElement.parentNode.removeChild(this.$audioElement);
            }
            if(this.$imageElement != undefined){
                this.$imageElement.parentNode.removeChild(this.$imageElement);
            }
        }
    })


};


const ScenarioManager = {
    
    
    createScenario : (name) => ({
        name,
        returnObj:{},

        loadFromMountingTable(){
            this.returnObj.name = this.name;
            this.returnObj.duration = MountingTable.duration;
            this.returnObj.scale = MountingTable.scale;
            this.returnObj.tracks = [];
            MountingTable.tracks.forEach(track => {
                let trackObj = {};
                trackObj.id = track.id;
                trackObj.actorid = track.actorID;
                trackObj.actorname = track.actorName;
                trackObj.sources = [];
                track.sources.forEach(source => {
                    let sourceObj = {};
                    sourceObj.name = source.name;
                    sourceObj.type = source.type;
                    sourceObj.position = source.position;
                    sourceObj.duration = source.duration;
                    trackObj.sources.push(sourceObj);
                });
                this.returnObj.tracks.push(trackObj);
            });

            
        }


    }),
    saveScenario : (name) => {
        ScenarioManager.scenario = ScenarioManager.createScenario(name);
        
        ScenarioManager.scenario.loadFromMountingTable();
        
        
    },
    importScenario : (data) => {
        console.log(data);
        MountingTable.unselectSources();
        MountingTable.$dialog.style.display = 'none';
        let currentDuration = parseFloat(MountingTable.duration);
        UI.setInput('mounting-table', 'duration', currentDuration + parseFloat(data.duration));
        MountingTable.setDuration(currentDuration + parseFloat(data.duration));
        data.tracks.forEach(dataTrack => {
            console.log(dataTrack);
            let track = MountingTable.getTrackFromActorId(dataTrack.actorid);
            if(track == false){
                track = MountingTable.addTrack();
            }
            let $actorsList = track.$trackUI.querySelector('select.actors-list');
            //add node in select for track if not already there
            if($actorsList.querySelector('option[value="'+dataTrack.actorid+'"]') == null){
                let $option = document.createElement('option');
                $option.value = dataTrack.actorid;
                $option.innerText = dataTrack.actorname;
                $actorsList.appendChild($option);
            }

            $actorsList.value = dataTrack.actorid;
            track.actorID = dataTrack.actorid;
            track.actorName = dataTrack.actorname;
            dataTrack.sources.forEach(dataSource => {
                console.log('adding source'+dataSource.name);
                track.addSource(dataSource.type, dataSource.duration, dataSource.name, currentDuration+dataSource.position);
            });

        });
    },
    loadScenario : (data) => {
        console.log(data);
        
        MountingTable.reset();
        MountingTable.show();
        MountingTable.initDurationAndScale(data.duration, data.scale);
        ScenarioManager.scenario = ScenarioManager.createScenario(data.name);
        UI.setInput('mounting-table', 'duration', data.duration);
        UI.setInput('mounting-table', 'scale', data.scale);
        data.tracks.forEach(dataTrack => {
            console.log(dataTrack);
            let track = MountingTable.addTrack();
            let $actorsList = track.$trackUI.querySelector('select.actors-list');
            //add node in select for track if not already there
            if($actorsList.querySelector('option[value="'+dataTrack.actorid+'"]') == null){
                let $option = document.createElement('option');
                $option.value = dataTrack.actorid;
                $option.innerText = dataTrack.actorname;
                $actorsList.appendChild($option);
            }
            $actorsList.value = dataTrack.actorid;
            track.actorID = dataTrack.actorid;
            track.actorName = dataTrack.actorname;
            dataTrack.sources.forEach(dataSource => {
                track.addSource(dataSource.type, dataSource.duration, dataSource.name, dataSource.position);
            });

        });

    }

};

const KeyboardManager = {
    init:() => {
        KeyboardManager.keysPressed = [];
        document.addEventListener('keydown', (e) => {
            console.log(e.key);
            switch(e.key){
                case 'Delete': case 'Backspace':
                    MountingTable.onAction('delete');
                    break;
                case 'Control': case 'Shift':
                    KeyboardManager.keysPressed = [];
                    break;
                
            }
            if(KeyboardManager.keysPressed.indexOf(e.key) === -1)
            KeyboardManager.keysPressed.push(e.key);

            console.log(KeyboardManager.keysPressed);

            KeyboardManager.shortcuts();

            
        });
        document.addEventListener('keyup', (e) => {
            KeyboardManager.keysPressed = KeyboardManager.keysPressed.filter(v => v !== e.key);
        });
    },
    shortcuts:() => {
        let keyCombo = KeyboardManager.keysPressed.join();
        console.log(keyCombo);
        switch(keyCombo){
            case 'Control,C': case 'Control,c':
                MountingTable.onAction('copy');
                break;
            case 'Control,V': case 'Control,v':
                MountingTable.onAction('paste');
                break;
            case 'Shift,I': case 'Shift,i':
                MountingTable.onAction('import');
                break;
        }
    }

};

UI.update(document);
SocketManager.init();
MountingTable.init();
KeyboardManager.init();
