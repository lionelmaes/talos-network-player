import json

choirSrc = {'CLA':[], 'DEB': [], 'ELS':[], 'SMA':[], 'ALL':[]}

choirTranslate = ['CLA', 'DEB', 'ELS', 'SMA']

with open('data.json', 'r') as f:
    sources = json.load(f)

for source in sources:
    if source['file'][0:18] == 'OTLAS_SPEECH_CHOIR':
        choirSrc[source['file'][19:22]].append(source)


output = {'name':'_OTLAS_CHOIR_script01', "duration":2000,"scale":100, 'tracks':[{'id':0, 'actorid':'10.42.0.10', 'actorname':'talos1', 'sources':[]}, {'id':1, 'actorid':'10.42.0.11', 'actorname':'talos2', 'sources':[]}, {'id':2, 'actorid':'10.42.0.12', 'actorname':'talos3', 'sources':[]}, {'id':3, 'actorid':'10.42.0.13', 'actorname':'talos4', 'sources':[]}, {'id':4, 'actorid':'10.42.0.14', 'actorname':'talos5', 'sources':[]}]}


startTime = 0
actorInd = 0
for i in range(1, 59):
    startTrack = (i - 1) % 5
    print('step %s' % i)
    duration = 0
    for j in range(startTrack, startTrack + 3):
        print('track %s' % (j%5))
        print('actor %s' % (actorInd % 4))
        actorName = choirTranslate[actorInd % 4]
        print('source %s' % choirSrc[actorName][i -1])
        source = choirSrc[actorName][i -1]
        del source['moddate']
        source['position'] = startTime
        source['name'] = source['file']
        del source['file']
        duration = source['duration']
        output['tracks'][j%5]['sources'].append(source)
        actorInd += 1
    startTime += float(duration) + 3

print(output)
with open('_OTLAS_CHOIR_script01.json', 'w') as f:
    json.dump(output, f)
