#TODO:

#set block begin and duration
#move block from one track to another


from flask import Flask, render_template
import flask_socketio as ss
import time
import datetime
import config
from modules.ClientSockets import *
import threading
import server
import client
from socket import *
from netaddr import *
import netifaces as ni
import uuid
import subprocess
import json
import vlc
from pathlib import Path
from logger import logger
import alsaaudio



class PlayerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        #self.isPlaying = False
        self.vlcInstance = vlc.Instance('--aout=alsa --alsa-audio-device=hw --verbose=-1 --video-on-top')
        
        self.playEvent = threading.Event()
        self.playEvent.set()
        self.timers = []
        self.status = 'pause'
        self.nextSeanceTime = None
        self.players = []
        self.endSeanceTimer = None
        self.alsa = alsaaudio.Mixer('Digital', cardindex=0)
        
    def launchSequence(self):
        self.playEvent.clear()

        self.timers = []
        self.sequence = []
        self.players = []
        logger.info('launching sequence')
        for track in self.scenario['tracks']:
            if(track['actorid'] == config.IP):
                logger.debug(track['actorid'])
                self.sequence = track['sources']
                break
        
        for i, source in enumerate(self.sequence):
            sourceStartTime = self.nextSeanceTime + datetime.timedelta(seconds=float(source['position']))
            #same thing for video audio or image
            #if(source['type'] == 'video' or source['type'] == 'image'):
                
            try:
                player = self.vlcInstance.media_player_new()
                media = self.vlcInstance.media_new(Path('%s/%s' % (config.DATA_PATH, source['name'])))
                player.set_media(media)
                #media.add_option('start-time=0.0')
                #media.add_option('run-time=%s' % float(source['duration']))
                self.players.append(player)
                
            except:
                logger.warning('problem with players oh well continue')
            
            source['player'] = self.players[len(self.players) - 1]
            #time.sleep(1)
                

            logger.debug('source position %s' % float(source['position']))
            sourceEndTime = sourceStartTime +  datetime.timedelta(seconds=float(source['duration']))

            logger.info('launching %s in %s' % (source['name'], (sourceStartTime - datetime.datetime.now()).total_seconds()))

            startTimer = threading.Timer((sourceStartTime - datetime.datetime.now()).total_seconds(), self.playSource, [source])
            
            endTimer = threading.Timer((sourceEndTime - datetime.datetime.now()).total_seconds(), self.stopSource, [source, startTimer])
            
            startTimer.start()
            endTimer.start()

            self.timers.append({'start':startTimer, 'end':endTimer})

        logger.debug(self.timers)
    
    def playSource(self, source):
        logger.info('playing %s. Type: %s' % (source['name'],  source['type']))
        #same thing for video audio or image
        #if(source['type'] == 'video' or source['type'] == 'image'):
        for player in self.players:
            player.stop()

        self.alsa.setvolume(94)
        source['player'].play()
        #this one is to prevent image overlap
        
                                

    def stopSource(self, source, timer):
        logger.info('stop')
        #if there is another player playing: do not switch off the volume!
        superposition = False
        for player in self.players:
            if source['player'] == player:
                continue

            if player.is_playing():
                logger.debug('superposition!')
                superposition = True
                break

        if not(superposition):
            self.alsa.setvolume(0)

        source['player'].stop()

        timer.cancel()

    def run(self):
        
        logger.info('playerthread launched')
        
        self.callForReload()
        
        counter = 0
        while True:
            #logger.debug('check for next seance. Is it playing? %s, Is there a seancetime? %s' % (not(self.playEvent.is_set()), config.SEANCETIME))
            #when playEvent is cleared, there is a seance scheduled we wait for it to be set. 
            #but if a new schedule is sent, 
            #self.playEvent.wait()
            if (config.SEANCETIME != None and self.status != 'pause'):
                #logger.debug('new seance time or playing seance')
                configTime = datetime.datetime.strptime(config.SEANCETIME, "%d-%b-%Y (%H:%M:%S.%f)")
                #if configTime is equal to self.nextSeanceTime: we are already playing this seance
                #else we need to launch a new seance (and stop the previous one)
                if(configTime != self.nextSeanceTime):
                    if(configTime <= datetime.datetime.now()):
                        logger.info('seance of the past. the clock server needs to plan a new one. let us wait for it')
                        time.sleep(2)
                        continue
                    logger.debug('new seance time!')
                    self.resetTimersAndPlayers()
                    self.nextSeanceTime = datetime.datetime.strptime(config.SEANCETIME, "%d-%b-%Y (%H:%M:%S.%f)")
            
                    logger.info('next seance scheduled for: %s' % (datetime.datetime.strftime(self.nextSeanceTime, "%d-%b-%Y (%H:%M:%S.%f)")))
                    endOfSeance = self.nextSeanceTime + datetime.timedelta(seconds=float(self.scenario['duration']))
                    logger.info('end of seance scheduled for: %s' % (datetime.datetime.strftime(endOfSeance, "%d-%b-%Y (%H:%M:%S.%f)")))
                       
                    logger.info('planning new seance')
                    self.endSeanceTimer = threading.Timer((endOfSeance - datetime.datetime.now()).total_seconds(), self.endSeance)
                    self.endSeanceTimer.start()
                    self.launchSequence()

            
            time.sleep(2)

    def resetTimersAndPlayers(self):
        logger.info('reset timers and players')
        if self.endSeanceTimer != None:
            self.endSeanceTimer.cancel()

        for player in self.players:
            player.stop()

        for timer in self.timers:
            timer['start'].cancel()
            timer['end'].cancel()


    def endSeance(self):
        logger.info('end of seance')
        self.resetTimersAndPlayers()
        config.SEANCETIME = None


        
       
    def callForReload(self):
        with open('status.json') as f:
                status = json.load(f)
        logger.info('loading status file')
        self.status= status['file']
        if(self.status != 'pause'):
            with open('%s/%s' % (config.SCENARIOS_PATH, self.status)) as f:
                self.scenario = json.load(f)
                logger.info(self.scenario)
                config.SCENARIO_DURATION = int(self.scenario['duration'])
        else:
            logger.info('pause!')
        if(self.endSeanceTimer != None):
            self.endSeanceTimer.cancel()
        self.endSeance()

        

       
    def synchronize(self, clients):
        logger.info("sync")
        
        
class ServerThread(threading.Thread):
    
    def planNewSeance(self):
        if(self.server.isClock):
            self.server.seanceTime = None
            clientsThread.clockClient.getNextSeance()
           

    def run(self):
        logger.info("server thread launched")
        self.server = server.Server(self)

        self.server.serverSocket.run(self.server.flaskServer, host='0.0.0.0', debug=True, use_reloader=False)
        


class ClientsThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.queryNextSeance = threading.Event()
        self.callForReloadEvent = threading.Event()
        self.clients = []
        self.selfClient = None
        self.clockClient = None
        
        self.iprange = IPRange(config.IP_START, config.IP_END)
        

    def getClientFromIP(self, ip):
        for client in self.clients:
            if client.serverIP == ip:
                return True
        return False

    def run(self):
        self.nextSeance = None


        while True:
            
            #if we need to query next seance time 
            if self.queryNextSeance.is_set() and self.clockClient != None:
                logger.debug('need to query next seance')
                self.clockClient.getNextSeance()
                self.queryNextSeance.clear()
            
            #if we need to reload the score
            for c in self.clients:
                if c.playFileEvent.is_set():
                    logger.debug('we changed the score. call for reload!')
                    self.callForReloadEvent.set()
                    c.playFileEvent.clear()
                    break

            #update client list with their status
            self.clients = [c for c in self.clients if c.status == 'up']

            #remove up servers ips from iprange list
            ipsToScan = [ip for ip in self.iprange if not self.getClientFromIP(ip)]
            


            #look up for servers
            for ip in ipsToScan:
                newClient = client.Client(ip, config.PORT)

                if(newClient.connect()):
                    #wait for the server to respond
                    newClient.serverConnectionEvent.wait()
                    
                    
                    #logger.debug('new client instanciated! %s' % str(ip))            
                    if(str(ip) == config.IP):
                        logger.info("this a self client!")
                        self.selfClient = newClient
                    
                    self.clients.append(newClient)

                
                #self.port_scan(config.PORT, ip)
            config.CLIENTS = self.clients
            #logger.debug('clients found')
            #logger.debug(self.clients)
            #finally we set the clockClient

            if self.selfClient != None:
                if self.clockClient == None or self.clockClient.status == 'down':
                    clockClient = self.selfClient
                else:
                    #we need to check if there is a more valid clockClient
                    clockClient = self.clockClient
                
                for c in config.CLIENTS:
                    if c.serverClockContest >= clockClient.serverClockContest:
                        
                        clockClient = c
                
                if clockClient != self.clockClient:
                    logger.info('setting new clock. congrats to %s' % (clockClient.serverHostName))
                    if(self.clockClient != None):
                        self.clockClient.setClockDown()
                    self.clockClient = clockClient
                    self.clockClient.setClockUp()   
                    logger.debug('we have changed clock, callForReload!')
                    self.callForReloadEvent.set()
                    



            time.sleep(3)
            

def getHostNameIP(): 

    hostName = gethostname()
    ni.ifaddresses('eth0')
    hostIP = ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']
    
    return {'hostname':hostName, 'ip':hostIP}
   
def syncClock():
    if(clientsThread.clockClient != None and str(clientsThread.clockClient.serverIP) != config.IP):
        logger.info('sync with clock set to: %s' % (clientsThread.clockClient.serverHostName))
    
        result = subprocess.run(["sudo", "sntp", "-s", str(clientsThread.clockClient.serverIP)], capture_output=True)


if __name__ == '__main__':
    logger.info("main thread launched")
    
    #INIT CONFIG
    nodeInfo = getHostNameIP()

    config.IP = nodeInfo['ip']
    config.HOSTNAME = nodeInfo['hostname']
    config.CLOCKCONTEST = uuid.uuid4().int
    #config.ISCLOCK = False
    config.SEANCETIME = None
    config.SCENARIO_DURATION = 0
    config.CLIENTS = []
    logger.info('clock contest: %s' % (config.CLOCKCONTEST))


    
    #INIT THREADS
    serverThread = ServerThread()
    clientsThread = ClientsThread()
    playerThread = PlayerThread()
    
    time.sleep(3)

    clientsThread.start()
    serverThread.start()
    playerThread.start()
    
    secondsElapsed = 0
    while True:
        
        if secondsElapsed == 10:
            syncClock()
            secondsElapsed = 0

        if config.SEANCETIME == None and playerThread.status != 'pause': 
            #logger.debug('calling for next seance')
            clientsThread.queryNextSeance.set()

        if clientsThread.callForReloadEvent.is_set():
            playerThread.callForReload()
            clientsThread.callForReloadEvent.clear()

        #playerThread.synchronize(clientsThread.clients)
        secondsElapsed += 1
        time.sleep(1)

    