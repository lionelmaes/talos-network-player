from flask import Flask, render_template
import flask_socketio as ss
import time
import datetime
import config
import os
from modules.ClientSockets import *
import threading
import json
from os import listdir
from os.path import isfile, join
from ffprobe import FFProbe
import mimetypes
from logger import logger

class Server:
    def __init__(self, serverThread):
        mimetypes.init()
        logger.info('new server init')
        async_mode = None
        self.serverThread = serverThread
        
        self.flaskServer = Flask(__name__)
        self.serverSocket = ss.SocketIO(self.flaskServer, async_mode=async_mode)
        self.isClock = False
        self.scenarioFiles = []
        self.dataFiles = []
        self.dataSources = []

        self.loadFiles()

        @self.flaskServer.route('/')
        def index():
            logger.debug('building index')
            self.scenarioFiles = [f for f in sorted(listdir(config.SCENARIOS_PATH)) if isfile(join(config.SCENARIOS_PATH, f))]
        
            return render_template('index.html', hostname = config.HOSTNAME, sources = self.dataSources, scenarios = self.scenarioFiles, async_mode=self.serverSocket.async_mode)

        @self.serverSocket.on('node_connection')
        def onNodeConnection(data):
            logger.info('ohoooo I see a new node:'+data['ip'])
            
            ss.emit('server_connection', {'serverHostName':config.HOSTNAME, 'serverClockContest':config.CLOCKCONTEST})
            ss.emit('new_node', {'clientIP':data['ip'], 'clientHostName':data['name'], 
            'serverHostName':config.HOSTNAME, 'serverIP':config.IP}, broadcast=True)

        @self.serverSocket.on('interface_connection')
        def onInterfaceConnection():
            logger.info('welcome, human')
            ss.emit('node_list', [{'serverHostName':c.serverHostName, 
            'serverIP':str(c.serverIP), 'clientHostName':c.clientHostName,
            'clientIP':str(c.clientIP)} for c in config.CLIENTS])
            logger.info('clients: %s' % config.CLIENTS)

        @self.serverSocket.on('save_scenario')
        def onSaveScenario(data):
            ss.emit('save_scenario', data, broadcast = True)
            logger.info('broadcasting scenario to clients')

       
        @self.serverSocket.on('load_scenario')
        def loadScenario(data):
            with open('%s/%s' % (config.SCENARIOS_PATH, data['file'])) as f:
                scenario = json.load(f)
            ss.emit('load_scenario', scenario)
        
        @self.serverSocket.on('import_scenario')
        def loadScenario(data):
            with open('%s/%s' % (config.SCENARIOS_PATH, data['file'])) as f:
                scenario = json.load(f)
            ss.emit('import_scenario', scenario)

        @self.serverSocket.on('play_file')
        def playFile(data):
            logger.info("sending player status to clients")
            ss.emit('play_file', data, broadcast = True)

        '''@self.serverSocket.on('set_clock_up')
        def setClockUp():
            config.ISCLOCK = True

        @self.serverSocket.on('set_clock_down')
        def setClockDown():
            config.ISCLOCK = False'''

        @self.serverSocket.on('get_next_seance')
        def getNextSeance():
            #compute seance time if not defined as now + delay
            #or as seancetime + seanceduration + delay if seancetime is in the past
            
            if config.SEANCETIME == None:
                config.SEANCETIME = (datetime.datetime.now()+datetime.timedelta(seconds=20)).strftime("%d-%b-%Y (%H:%M:%S.%f)")
            else:
                currentSeanceTime = datetime.datetime.strptime(config.SEANCETIME, "%d-%b-%Y (%H:%M:%S.%f)")
                logger.info("current seance time: %s " % currentSeanceTime)
                logger.info("now + 20 seconds %s" % str(datetime.datetime.now() + datetime.timedelta(seconds=20)))
                
                if currentSeanceTime <= datetime.datetime.now() + datetime.timedelta(seconds=20):
                    logger.info('seance of the past, updating new seancetime')
                    #this one was meant to wait the current seance to end before launching a new one 
                    #config.SEANCETIME = (currentSeanceTime+datetime.timedelta(seconds=config.SCENARIO_DURATION+60)).strftime("%d-%b-%Y (%H:%M:%S.%f)")
                    config.SEANCETIME = (datetime.datetime.now()+datetime.timedelta(seconds=20)).strftime("%d-%b-%Y (%H:%M:%S.%f)")

        
            logger.info('sending new seance time %s ' % config.SEANCETIME)
            #when somebody asks for seance time, the server broadcasts it to all the clients
            ss.emit('receive_next_seance', {'time':config.SEANCETIME, 'hostName':config.HOSTNAME}, broadcast = True)

    def loadFiles(self):
        logger.debug('loading files')
       

            
        self.dataFiles = [f for f in sorted(listdir(config.DATA_PATH)) if isfile(join(config.DATA_PATH, f))]
        self.dataSources = []

        with open(config.DATA_SOURCES_PATH) as f:
            try:
                knownSources = json.load(f)
            except:
                knownSources = []
        

        for f in self.dataFiles:
            logger.debug(f)
            mimestart = mimetypes.guess_type(f)[0]
            logger.debug('type guessed')
            if mimestart != None:
                mimestart = mimestart.split('/')[0]
                moddate = os.stat(join(config.DATA_PATH, f))[8]
                logger.debug('file mod date: %s' % moddate)
                source = False
                #check if the source is known
                for knownSource in knownSources:
                    if knownSource['file'] == f and knownSource['moddate'] == moddate:
                        source = knownSource
                        
                if source == False:  
                    source = {'file':f, 'type':mimestart, 'moddate':moddate}
                    if mimestart == 'video' or mimestart == 'audio':
                        try:
                            source['duration'] = FFProbe(join(config.DATA_PATH, f)).streams[0].duration
                        except:
                            source['duration'] = 10
                    else:
                        source['duration'] = 10
                
                self.dataSources.append(source)
            
            with open(config.DATA_SOURCES_PATH, 'w') as f:
                json.dump(self.dataSources, f)
            
