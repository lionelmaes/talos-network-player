import vlc
import time 
import alsaaudio
vlcInstance = vlc.Instance('--aout=alsa --alsa-audio-device=hw --verbose=-1')
player = vlcInstance.media_player_new()

alsa = alsaaudio.Mixer()
alsa.setvolume(100)
media = vlcInstance.media_new("data/OTLAS_SPEECH_CHOIR_SMA (03).mp4")
#media.add_option('start-time=0.0')
#media.add_option('run-time=1.5')
player.set_media(media)
player.play()
time.sleep(2)
print('stop!')
alsa.setvolume(0)
player.stop()
