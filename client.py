
import socketio
import asyncio
#import socketIO_client as sc
import time
import json
import config
import threading
from logger import logger

class Client:
    def __init__(self, serverIP, port):
        
        self.serverIP = serverIP
        self.clientIP = config.IP
        self.clientHostName = config.HOSTNAME
        self.sio = socketio.Client()
        self.port = port
        self.status = 'down'
        self.isClock = False
        self.serverClockContest = None
        self.serverConnectionEvent = threading.Event()
        self.playFileEvent = threading.Event()

        @self.sio.on('connect')
        def on_connect():
            self.status = 'up'
        
        @self.sio.on('disconnect')
        def on_disconnect():
            logger.debug('disconnected from %s' % self.serverHostName)
            self.status = 'down'

        @self.sio.on('server_connection')
        def on_server_connect(data):
            self.serverHostName = data['serverHostName']
            self.serverClockContest = data['serverClockContest']
            self.serverConnectionEvent.set()
            
        
        @self.sio.on('message_from_server')
        def on_message(args):
            logger.info('message from server %s' % args['text'])
        #print(config.IP_RANGE)

        @self.sio.on('play_file')
        def on_play_file(data):
            with open('status.json', 'w') as f:
                json.dump(data, f)
            logger.debug('saved new status')
            self.playFileEvent.set()

        @self.sio.on('save_scenario')
        def on_save_scenario(data):
            logger.info('received new scenario')
            with open('%s/%s.json' % (config.SCENARIOS_PATH, data['name']), 'w') as f:
                json.dump(data, f)

        @self.sio.on('receive_next_seance')
        def receiveNextSeance(data):
            logger.info('receiving new seance from clock: %s' % data['time'])
            if(self.isClock):
                config.SEANCETIME = data['time']   
            else:
                logger.debug('received seance time from another clock')

    def getNextSeance(self):
        self.sio.emit('get_next_seance')

    def setClockUp(self):
        self.isClock = True
        #self.sio.emit('set_clock_up')

    def setClockDown(self):
        self.isClock = False
        #self.sio.emit('set_clock_down')

    def connect(self):
        #print("trying to connect to http://%s:%s" % (self.serverIP, self.port))
        try:
            #logger.debug('connection attempt!')
            self.sio.connect('http://%s:%s' % (self.serverIP, self.port))
            self.sio.emit('node_connection', {'ip':config.IP, 'name':config.HOSTNAME, 'clock-contest':config.CLOCKCONTEST})
            return True
        except:
            return False
        #await sio.call('connection', {'text': 'hello there'})
        #await sio.emit('connection', {'text': 'hello there'}, callback=done)
        #print('connection ok')
        #self.clients.append(sio)
        #await sio.wait()