from omxplayer.player import OMXPlayer
from pathlib import Path
import time

try:
    player1 = OMXPlayer(Path('data/crunch1.mp4'),  dbus_name='org.talos.mplayer.omxplayer1', args=['--no-osd', '--no-keys', '-b', '-o', 'alsa', '--layer', 1])
    player2 = OMXPlayer(Path('data/talosvid.mp4'),  dbus_name='org.talos.mplayer.omxplayer2', args=['--no-osd', '--no-keys', '-b', '-o', 'alsa', '--layer', 2])
    while True:
        if(player1.can_play() and player2.can_play()):
            break
        time.sleep(1)
    time.sleep(2)
    while True:
        player1.play()
        player2.pause()
        time.sleep(2)
        player1.pause()
        player2.play()
        time.sleep(2)
        
        
        
    #player.play_sync()
   
    
except KeyboardInterrupt:
    player1.quit()
    player2.quit()
    print("goodbye")