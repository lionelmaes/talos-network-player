from socketIO_client import SocketIO, LoggingNamespace

def on_message(args):
    print('messagefromserver', args['text'])


with SocketIO('10.42.0.10', 5000, LoggingNamespace) as socketIO:
    socketIO.on('message_from_server', on_message)
    socketIO.emit('connection', {'text':'hello there'})
    socketIO.wait(seconds=1)